package kdtree

import (
	"testing"
)

var n = []*BaseNode{
	{
		Values: []float64{
			4,
			7,
		},
	},
	{
		Values: []float64{
			8,
			1,
		},
	},
	{
		Values: []float64{
			2,
			3,
		},
	},
	{
		Values: []float64{
			7,
			2,
		},
	},
	{
		Values: []float64{
			5,
			4,
		},
	},
	{
		Values: []float64{
			9,
			6,
		},
	},
}

var tree = NewTree(n, 0)

//func TestNewTree(t *testing.T) {
/*
	n := make([]*BaseNode, 28000000)
	for i := 0; i < 28000000; i++ {
		x := rand.Float64()
		y := rand.Float64()
		v := []float64{x, y}
		node := &BaseNode{
			Values: v,
		}
		n[i] = node

	}
*/

//printTree(tree.root)
//dumpStdout(tree.root, "")
//}

func TestTree_Within(t *testing.T) {

	buf := &NodeBuffer{}
	err := tree.Within([]float64{
		7.,
		6.,
	}, 2., buf)

	if err != nil {
		t.Error(err)
	}

	if len(buf.Content) != 1 {
		t.Fail()
	}

	first := buf.Content[0]
	if first.Values[0] != 9. || first.Values[1] != 6 {
		t.Fail()
	}

}

func TestTree_InRect(t *testing.T) {
	buf := &NodeBuffer{}
	rect := &Rect{
		botLeft: []float64{
			1.,
			1.,
		},
		topRight: []float64{
			6.,
			5.,
		},
	}

	err := tree.InRect(rect, buf)

	if err != nil {
		t.Error(err)
	}

	if len(buf.Content) != 2 {
		t.Fail()
	}

	first := buf.Content[0]

	if first.Values[0] != 5 || first.Values[1] != 4 {
		t.Fail()
	}

	second := buf.Content[1]
	if second.Values[0] != 2 || second.Values[1] != 3 {
		t.Fail()
	}

}

func TestTree_InRect2(t *testing.T) {
	buf := &NodeBuffer{}
	rect := &Rect{
		botLeft: []float64{
			3.,
			6.,
		},
		topRight: []float64{
			7.,
			8.,
		},
	}

	err := tree.InRect(rect, buf)

	if err != nil {
		t.Error(err)
	}

	if len(buf.Content) != 1 {
		t.Fail()
	}

	first := buf.Content[0]

	if first.Values[0] != 4 || first.Values[1] != 7 {
		t.Fail()
	}
}

func TestTree_InRect3(t *testing.T) {
	buf := &NodeBuffer{}
	rect := &Rect{
		botLeft: []float64{
			8.,
			5.,
		},
		topRight: []float64{
			10.,
			7.,
		},
	}

	err := tree.InRect(rect, buf)

	if err != nil {
		t.Error(err)
	}

	if len(buf.Content) != 1 {
		t.Fail()
	}

	first := buf.Content[0]

	if first.Values[0] != 9 || first.Values[1] != 6 {
		t.Fail()
	}
}

func TestTree_InRect4(t *testing.T) {
	buf := &NodeBuffer{}
	rect := &Rect{
		botLeft: []float64{
			3.,
			3.,
		},
		topRight: []float64{
			6.,
			8.,
		},
	}

	err := tree.InRect(rect, buf)

	if err != nil {
		t.Error(err)
	}

	if len(buf.Content) != 2 {
		t.Fail()
	}

	first := buf.Content[0]

	if first.Values[0] != 5 || first.Values[1] != 4 {
		t.Fail()
	}

	second := buf.Content[1]
	if second.Values[0] != 4 || second.Values[1] != 7 {
		t.Fail()
	}
}

func TestTree_InRect5(t *testing.T) {
	buf := &NodeBuffer{}
	rect := &Rect{
		botLeft: []float64{
			1.,
			2.,
		},
		topRight: []float64{
			5.,
			8.,
		},
	}

	err := tree.InRect(rect, buf)

	if err != nil {
		t.Error(err)
	}

	if len(buf.Content) != 3 {
		t.Fail()
	}

	first := buf.Content[0]

	if first.Values[0] != 5 || first.Values[1] != 4 {
		t.Fail()
	}

	second := buf.Content[1]
	if second.Values[0] != 2 || second.Values[1] != 3 {
		t.Fail()
	}

	third := buf.Content[2]
	if third.Values[0] != 4 || third.Values[1] != 7 {
		t.Fail()
	}
}
