package kdtree

import (
	"math"
)

// Node represents an entry within the tree
type Node interface {
	Dimension() int
}

// BaseNode implements Node
type BaseNode struct {
	Content    interface{}
	Values     []float64
	LeftChild  *BaseNode
	RightChild *BaseNode
	Axis       int
}

const (
	EARTH_RADIUS = 6371
)

// Dimension returns the dimension of the node
func (n *BaseNode) Dimension() int {
	return len(n.Values)
}

func (n *BaseNode) within(center []float64, radius float64, buffer *NodeBuffer) {

	if n.DistanceTo(center) <= radius {
		buffer.Put(n)
	}

	if center[n.Axis] <= n.Values[n.Axis] {
		if n.LeftChild != nil {
			n.LeftChild.within(center, radius, buffer)
		}

		if n.RightChild != nil && center[n.Axis]+radius >= n.Values[n.Axis] {
			n.RightChild.within(center, radius, buffer)
		}
	} else {
		if n.RightChild != nil {
			n.RightChild.within(center, radius, buffer)
		}
		if n.LeftChild != nil && center[n.Axis]-radius <= n.Values[n.Axis] {
			n.LeftChild.within(center, radius, buffer)
		}
	}

}

func (n *BaseNode) withinWalk(center []float64, radius float64, f WithinFunc) {
	if n.DistanceTo(center) <= radius {
		f(n)
	}


	if center[n.Axis] <= n.Values[n.Axis] {
		if n.LeftChild != nil {
			n.LeftChild.withinWalk(center, radius, f)
		}

		if n.RightChild != nil && center[n.Axis]+radius >= n.Values[n.Axis] {
			n.RightChild.withinWalk(center, radius, f)
		}
	} else {
		if n.RightChild != nil {
			n.RightChild.withinWalk(center, radius, f)
		}
		if n.LeftChild != nil && center[n.Axis]-radius <= n.Values[n.Axis] {
			n.LeftChild.withinWalk(center, radius, f)
		}
	}
}

// DistanceTo calculates the distance of a node to another coordinate
func (n *BaseNode) DistanceTo(other []float64) float64 {
	dim := n.Dimension()
	if dim != len(other) {
		return .0
	}

	var p float64
	for i := 0; i < dim; i++ {
		d := n.Values[i] - other[i]
		p += (d * d)
	}

	return math.Sqrt(p)
}

// GreatCircleDistance returns the distance in kilometers
func (n *BaseNode) GreatCircleDistance(other []float64, latIndex, lngIndex int) float64 {
	if n.Dimension() < 2 || len(other) < 2 {
		return .0
	}

	dLat := (other[latIndex] - n.Values[latIndex]) * (math.Pi / 180.0)
	dLng := (other[lngIndex] - n.Values[lngIndex]) * (math.Pi / 180.0)
	lat1 := n.Values[latIndex] * (math.Pi / 180.0)
	lat2 := other[latIndex] * (math.Pi / 100.0)

	a1 := math.Sin(dLat/2) * math.Sin(dLat/2)
	a2 := math.Sin(dLng/2) * math.Sin(dLng/2) * math.Cos(lat1) * math.Cos(lat2)

	a := a1 + a2

	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))

	return EARTH_RADIUS * c
}


func (n *BaseNode) withinGC(center []float64, radius float64, latIndex, lngIndex int, buffer *NodeBuffer) {

	if n.GreatCircleDistance(center, latIndex, lngIndex) <= radius {
		buffer.Put(n)
	}

	if center[n.Axis] <= n.Values[n.Axis] {
		if n.LeftChild != nil {
			n.LeftChild.within(center, radius, buffer)
		}

		if n.RightChild != nil && center[n.Axis]+radius >= n.Values[n.Axis] {
			n.RightChild.within(center, radius, buffer)
		}
	} else {
		if n.RightChild != nil {
			n.RightChild.within(center, radius, buffer)
		}
		if n.LeftChild != nil && center[n.Axis]-radius <= n.Values[n.Axis] {
			n.LeftChild.within(center, radius, buffer)
		}
	}

}

func (n *BaseNode) inRect(rect *Rect, buffer *NodeBuffer) {
	contained := rect.contains(n.Values)

	if contained {
		buffer.Put(n)
	}

	if n.LeftChild != nil {
		n.LeftChild.inRect(rect, buffer)
	}
	if n.RightChild != nil {
		n.RightChild.inRect(rect, buffer)
	}

	/*
	if contained {
		buffer.Put(n)
		if n.LeftChild != nil {
			n.LeftChild.inRect(rect, buffer)
		}
		if n.RightChild != nil {
			n.RightChild.inRect(rect, buffer)
		}
	} else { // TODO Check this with more tests (Worst case search all nodes...)
		//if rect.topRight[n.axis] <= n.Values[n.axis] {
			if n.LeftChild != nil {
				n.LeftChild.inRect(rect, buffer)
			}
		//} else {
			if n.RightChild != nil {
				n.RightChild.inRect(rect, buffer)
			}
		//}
	}
	*/
}

type sortableNodes []*BaseNode

func (s sortableNodes) Len() int {
	return len(s)
}

func (s sortableNodes) Less(i, j int) bool {
	return s[i].Values[curSortAxis] < s[j].Values[curSortAxis]
}

func (s sortableNodes) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
