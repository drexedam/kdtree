package kdtree

import (
	"fmt"
	"io"
	"math"
	"os"
)

type emptyInput struct {
	msg string
}

func (e emptyInput) Error() string {
	return e.msg
}

func median(nodes []*BaseNode) (median *BaseNode, index int, err error) {
	l := len(nodes)
	if l == 0 {
		return nil, -1, emptyInput{"Input list is empty"}
	}

	medI := int(math.Floor(float64(l) / 2.))
	return nodes[medI], medI, nil
}

func printTree(node *BaseNode) {
	if node == nil {
		return
	}
	queue := make([]*BaseNode, 0)
	queue = append(queue, node)

	nodesInCurrentLevel := 1
	nodesInNextLevel := 0

	for len(queue) > 0 {
		n := queue[0]
		queue = queue[1:]
		nodesInCurrentLevel--
		if n != nil {
			fmt.Print(n.Values)
			fmt.Print(" ")
			queue = append(queue, n.LeftChild)

			queue = append(queue, n.RightChild)

			nodesInNextLevel += 2
		} else {
			fmt.Print("nil")
			fmt.Print(" ")
		}

		if nodesInCurrentLevel == 0 {
			fmt.Println()
			nodesInCurrentLevel = nodesInNextLevel
			nodesInNextLevel = 0
		}
	}
}

func dumpStdout(n *BaseNode, prefix string) error {
	return dump(os.Stdout, n, prefix)
}

func dump(w io.Writer, n *BaseNode, prefix string) (err error) {
	str := fmt.Sprintf("+%v", n.Values)
	strlen := len(str)

	if _, err = io.WriteString(w, str); err != nil {
		return
	}

	var space string
	var minus string
	for i := 0; i < strlen-1; i++ {
		space += ` `
		minus += `-`
	}

	if n.LeftChild != nil {
		if err = dump(w, n.LeftChild, prefix+space+`|`); err != nil {
			return
		}
	} else {
		if _, err = io.WriteString(w, "+nil\n"); err != nil {
			return
		}
	}

	if _, err = io.WriteString(w, prefix+space); err != nil {
		return
	}

	if n.RightChild != nil {
		if err = dump(w, n.RightChild, prefix+` `+space); err != nil {
			return
		}
	} else {
		if _, err = io.WriteString(w, "+nil\n"); err != nil {
			return
		}
	}

	return
}
