package kdtree

// Rect represents a rectangle defined by its bottom left and upper right corner
type Rect struct {
	botLeft  []float64
	topRight []float64
}

func NewRect(bottomLeft []float64, topRight []float64) *Rect {
	return &Rect{
		botLeft: bottomLeft,
		topRight: topRight,
	}
}

func (r Rect) contains(v []float64) bool {
	if len(v) != len(r.botLeft) {
		return false
	}

	for i, val := range v {
		if val < r.botLeft[i] || val > r.topRight[i] {
			return false
		}
	}

	return true
}
