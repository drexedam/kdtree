package kdtree

import (
	"fmt"
	"sort"
)

// Tree represents a kD Tree
type Tree struct {
	Root  *BaseNode
	Nodes int
}

var (
	curSortAxis int
)

// NewTree creates a tree based on a given list of nodes (normally depth should be 0)
func NewTree(nodes []*BaseNode, depth int) *Tree {
	if len(nodes) < 1 {
		return nil
	}

	return &Tree{
		Root: createTree(nodes, depth),
		Nodes: len(nodes),
	}
}

func createTree(nodes []*BaseNode, depth int) *BaseNode {

	// We are done
	if len(nodes) < 1 {
		return nil
	}

	// Assume all points have the same dimension
	k := nodes[0].Dimension()

	// Select axis
	axis := depth % k

	// We need this for sorting
	curSortAxis = axis

	// Sort the list
	sort.Sort(sortableNodes(nodes))

	// Get median element and index
	n, i, _ := median(nodes)
	// Set node's axis
	n.Axis = axis
	// Repeat for all nodes left of median
	n.LeftChild = createTree(nodes[:i], depth+1)
	//Repeat for all nodes right of median
	n.RightChild = createTree(nodes[i+1:], depth+1)
	return n
}

func (t Tree) status() {
	fmt.Println(t.Root.Values)
}

// Within searches for all points within a radius around the given center and writes them into the provided buffer
func (t *Tree) Within(center []float64, radius float64, buffer *NodeBuffer) error {
	if t.Root == nil {
		return nil
	}

	if t.Root.Dimension() != len(center) {
		return ErrDimensionMismatch
	}

	t.Root.within(center, radius, buffer)

	return nil
}

func (t *Tree) WithinGC(center []float64, radius float64, latIndex, lngIndex int, buffer *NodeBuffer) error {
	if t.Root == nil {
		return nil
	}

	if t.Root.Dimension() < 2 || len(center) < 2 {
		return ErrDimensionMismatch
	}

	t.Root.withinGC(center, radius, latIndex, lngIndex, buffer)

	return nil
}


type WithinFunc func(*BaseNode)

func (t *Tree) WithinWalk(center []float64, radius float64, f WithinFunc) error {
	if t.Root == nil {
		return nil
	}

	if t.Root.Dimension() != len(center) {
		return ErrDimensionMismatch
	}

	f(t.Root)
	t.Root.withinWalk(center, radius, f)
	return nil

}

// InRect searches for all points within a rectangle and writes them into the provided buffer
func (t *Tree) InRect(rect *Rect, buffer *NodeBuffer) error {
	if t.Root == nil {
		return nil
	}

	if t.Root.Dimension() != len(rect.botLeft) {
		return ErrDimensionMismatch
	}

	t.Root.inRect(rect, buffer)

	return nil
}

func (t *Tree) Print() {
	printTree(t.Root)
}