package kdtree

// NodeBuffer is used for search operations on trees
type NodeBuffer struct {
	Content []*BaseNode
	off     int
}

// Put adds a value to the buffer
func (b *NodeBuffer) Put(n *BaseNode) {
	if b.off < len(b.Content) {
		b.Content[b.off] = n
	} else {
		b.Content = append(b.Content, n)
	}

	b.off++
}
