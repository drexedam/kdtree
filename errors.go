package kdtree

import "errors"

// ErrDimensionMismatch occurs if dimensions of two entities do not match
var ErrDimensionMismatch = errors.New("Dimensions do not match")
